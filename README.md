**POUŽITÍ:**

V souboru rest-example.http je vzorové využití aplikace v požadovaném pořadí - při vytváření entity Projection musí být zadáno existující ID entity Movie. Protože je strategie přidělování identifikačních čísel AUTO, musí být databáze před použitím vzorového použití prázdná/neexistující, aby se začala přidělovat ID od jedničky. 

**POŽADAVKY:**

* [x] Vše z 1.KB 
*       - opravena chyba nefunkčního GET projection/{id}
        - docházelo k chybě při vložení více než jedné entity, vyřešeno díky databázi
       
* [x] Použití persistentní relační DB 
*       - SQLite

* [x] Použití JPA, správné nastavení entit pomocí anotací a použití Spring Data JPA repozitářů
*       - snad ano, funguje to
       
* [x] Možnost přidat entity do "vztahů" přes HTTP requesty
*       - uživateli (customer) může být přiděleno promítání (projection) pomocí PUT users/{userId}/projections/{projectionId}
        - film (movie) může být změněn v promítání (projection) pomocí PUT projections/{projectionId}/movies/{movieId}

* [x] Práce umístěná na GIT
*       - zde

* [x] Zpřístupnění vzorových REST požadavků
*       - pomocí .http souboru



================================

Systém má tři entity:
    
1) zákazník (customer)
2) promítání (projetion)
3) film (movie)

================================

Vztahy entit:

1) zákazník a promítání (M:N):
    Zákazník může jít na libovolný počet promítání.
    Na jedno promítání může jít libovolný počet zákazníků (zatím, z implementačního hlediska, později asi přidám kapacitu)
    
2) promítání a film (M:1)
    Promítání má přiřazený vždy právě jeden film.
    Film může být na libovolném počtu promítání.
    
================================