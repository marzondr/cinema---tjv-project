package cz.cvut.fit.tjv.cinema.service;

import cz.cvut.fit.tjv.cinema.business.EntityStateException;
import cz.cvut.fit.tjv.cinema.business.MovieService;
import cz.cvut.fit.tjv.cinema.dao.MovieJpaRepository;
import cz.cvut.fit.tjv.cinema.domain.Movie;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MovieServiceTest {

    @InjectMocks
    MovieService movieService;

    @Mock
    MovieJpaRepository repository;

    @Test
    public void testReadAll() {
        Movie movie1 = new Movie(1L,"Harry Potter");
        Movie movie2 = new Movie(2L,"Red Notice");
        Movie movie3 = new Movie(3L,"Jumanji");

        List<Movie> movies = List.of(movie1, movie2, movie3);

        Mockito.when(repository.findAll()).thenReturn(movies);

        Collection<Movie> returnedMovies = movieService.readAll();

        assertEquals(3, returnedMovies.size());
        verify(repository, times(1)).findAll();
    }

    @Test
    public void testReadById() {
        Movie movie = new Movie(17L, "Lord of the Rings");

        Mockito.when(repository.findById(17L)).thenReturn(Optional.of(movie));
        Mockito.when(repository.findById(not(eq(17L)))).thenReturn(Optional.empty());

        Optional<Movie> returnedMovie = movieService.readById(17L);
        Optional<Movie> retEmptyMovie = movieService.readById(13L);

        assertTrue( returnedMovie.isPresent() );
        assertTrue( retEmptyMovie.isEmpty() );

        assertEquals( movie, returnedMovie.get() );
        verify(repository, times(2)).findById(any());
    }

    @Test
    public void testCreate() throws EntityStateException {
        Movie movie = new Movie(17L, "Lord of the Rings");
        movieService.create(movie);
        verify(repository, times(1)).save(movie);
    }

    @Test
    public void testUpdate() throws EntityStateException {
        Movie movie = new Movie(17L, "Lord of the Rings");

        Mockito.when(repository.existsById(17L)).thenReturn(true);
        Mockito.when(repository.existsById(not(eq(17L)))).thenReturn(false);

        try {
            movieService.update(new Movie(13L, "Not Existing Movie"));
        } catch (EntityStateException e) {
            verify(repository, never()).save(any());
        }

        movieService.update(movie);
        verify(repository, times(1)).save(movie);
    }

    @Test
    public void testDeleteById() {
        movieService.deleteById(17L);
        verify(repository, never()).deleteById(not(eq(17L)));
        verify(repository, times(1)).deleteById(17L);
    }
}
