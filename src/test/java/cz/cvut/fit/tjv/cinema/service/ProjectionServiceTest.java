package cz.cvut.fit.tjv.cinema.service;

import cz.cvut.fit.tjv.cinema.business.EntityStateException;
import cz.cvut.fit.tjv.cinema.business.ProjectionService;
import cz.cvut.fit.tjv.cinema.dao.ProjectionJpaRepository;
import cz.cvut.fit.tjv.cinema.domain.Movie;
import cz.cvut.fit.tjv.cinema.domain.Projection;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ProjectionServiceTest {

    @InjectMocks
    ProjectionService projectionService;

    @Mock
    ProjectionJpaRepository repository;

    @Test
    public void testReadAll() {
        Projection pro1 = new Projection(1L, LocalDateTime.of(1111, 1, 1, 1, 1, 1), new Movie(11L, "11"));
        Projection pro2 = new Projection(2L, LocalDateTime.of(2222, 2, 2, 2, 2, 2), new Movie(22L, "22"));
        Projection pro3 = new Projection(3L, LocalDateTime.of(3333, 3, 3, 3, 3, 3), new Movie(33L, "33"));

        List<Projection> projections = List.of(pro1, pro2, pro3);

        Mockito.when(repository.findAll()).thenReturn(projections);

        Collection<Projection> returnedProjections = projectionService.readAll();

        assertEquals(3, returnedProjections.size());
        verify(repository, times(1)).findAll();
    }

    @Test
    public void testReadByMovieId() {
        Projection pro1 = new Projection(1L, LocalDateTime.of(1111, 1, 1, 1, 1, 1), new Movie(11L, "11"));
        Projection pro2 = new Projection(2L, LocalDateTime.of(2222, 2, 2, 2, 2, 2), new Movie(11L, "22"));
        Projection pro3 = new Projection(3L, LocalDateTime.of(3333, 3, 3, 3, 3, 3), new Movie(11L, "33"));

        List<Projection> projections = List.of(pro1, pro2, pro3);

        Mockito.when(repository.readByMovieId(11L)).thenReturn(projections);

        Collection<Projection> returnedProjections = projectionService.readByMovieId(11L);

        assertEquals(3, returnedProjections.size());
        verify(repository, times(1)).readByMovieId(11L);
    }

    @Test
    public void testReadById() {
        Projection pro = new Projection(1L, LocalDateTime.of(1111, 1, 1, 1, 1, 1), new Movie(11L, "11"));

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(pro));
        Mockito.when(repository.findById(not(eq(1L)))).thenReturn(Optional.empty());

        Optional<Projection> returnedProjection = projectionService.readById(1L);
        Optional<Projection> retEmptyProjection = projectionService.readById(13L);

        assertTrue(returnedProjection.isPresent());
        assertTrue(retEmptyProjection.isEmpty());

        assertEquals(pro, returnedProjection.get());
        verify(repository, times(2)).findById(any());
    }

    @Test
    public void testCreate() throws EntityStateException {
        Projection pro = new Projection(1L, LocalDateTime.of(1111, 1, 1, 1, 1, 1), new Movie(11L, "11"));
        projectionService.create(pro);
        verify(repository, times(1)).save(pro);
    }

    @Test
    public void testUpdate() throws EntityStateException {
        Projection pro = new Projection(1L, LocalDateTime.of(1111, 1, 1, 1, 1, 1), new Movie(11L, "11"));

        Mockito.when(repository.existsById(1L)).thenReturn(true);
        Mockito.when(repository.existsById(not(eq(1L)))).thenReturn(false);

        try {
            projectionService.update(new Projection(13L, LocalDateTime.of(1111, 1, 1, 1, 1, 1), new Movie(11L, "11")));
        } catch (EntityStateException e) {
            verify(repository, never()).save(any());
        }

        projectionService.update(pro);
        verify(repository, times(1)).save(pro);
    }

    @Test
    public void deleteById() {
        projectionService.deleteById(1L);
        verify(repository, never()).deleteById(not(eq(1L)));
        verify(repository, times(1)).deleteById(1L);
    }

    @Test
    public void deleteTickets() {
        projectionService.deleteTickets(1L);
        verify(repository, never()).deleteTickets(not(eq(1L)));
        verify(repository, times(1)).deleteTickets(1L);
    }
}
