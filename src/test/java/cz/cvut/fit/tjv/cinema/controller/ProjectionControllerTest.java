package cz.cvut.fit.tjv.cinema.controller;

import cz.cvut.fit.tjv.cinema.api.controller.ProjectionController;
import cz.cvut.fit.tjv.cinema.business.MovieService;
import cz.cvut.fit.tjv.cinema.business.ProjectionService;
import cz.cvut.fit.tjv.cinema.domain.Movie;
import cz.cvut.fit.tjv.cinema.domain.Projection;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProjectionController.class)
public class ProjectionControllerTest {

    @MockBean
    ProjectionService projectionService;

    @MockBean
    MovieService movieService;

    @Autowired
    MockMvc mockMvc;

    @Test
    public void testGetAll() throws Exception {
        Projection pro1 = new Projection(1L, LocalDateTime.of(1111, 1, 1, 1, 1, 1), new Movie(11L, "11"));
        Projection pro2 = new Projection(2L, LocalDateTime.of(2222, 2, 2, 2, 2, 2), new Movie(22L, "22"));
        Projection pro3 = new Projection(3L, LocalDateTime.of(3333, 3, 3, 3, 3, 3), new Movie(33L, "33"));

        List<Projection> projections = List.of(pro1, pro2, pro3);

        Mockito.when(projectionService.readAll()).thenReturn(projections);

        mockMvc.perform(get("/projections"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(3)))
                .andExpect(jsonPath("$[0].id", Matchers.is(1)))
                .andExpect(jsonPath("$[1].id", Matchers.is(2)))
                .andExpect(jsonPath("$[2].id", Matchers.is(3)))
                .andExpect(jsonPath("$[0].date", Matchers.is("1111-01-01T01:01:01")))
                .andExpect(jsonPath("$[1].date", Matchers.is("2222-02-02T02:02:02")))
                .andExpect(jsonPath("$[2].date", Matchers.is("3333-03-03T03:03:03")));
    }

    @Test
    public void testGetOne() throws Exception {
        Projection pro = new Projection(1L, LocalDateTime.of(1111, 1, 1, 1, 1, 1), new Movie(11L, "11"));

        Mockito.when(projectionService.readById(1L)).thenReturn(Optional.of(pro));
        Mockito.when(projectionService.readById(not(eq(1L)))).thenReturn(Optional.empty());

        mockMvc.perform(get("/projections/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", Matchers.is(1)))
                .andExpect(jsonPath("$.date", Matchers.is("1111-01-01T01:01:01")))
                .andExpect(jsonPath("$.movie_id", Matchers.is(11)));

        mockMvc.perform(get("/projections/13"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testCreate() throws Exception {
        Movie movie = new Movie(11L, "11");
        Projection pro = new Projection(1L, LocalDateTime.of(1111, 1, 1, 1, 1, 1), movie);

        Mockito.when(projectionService.readById(any())).thenReturn(Optional.of(pro));
        Mockito.when(movieService.readById(11L)).thenReturn(Optional.of(movie));
        Mockito.when(movieService.readById(not(eq(11L)))).thenReturn(Optional.empty());

        mockMvc.perform(post("/projections")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"id\":\"1\",\"date\":\"1111-01-01T01:01:01\",\"movie_id\":\"11\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.date", Matchers.is("1111-01-01T01:01:01")))
                .andExpect(jsonPath("$.movie_id", Matchers.is(11)));

        ArgumentCaptor<Projection> argumentCaptor = ArgumentCaptor.forClass(Projection.class);
        verify(projectionService, Mockito.times(1)).create(argumentCaptor.capture());
        Projection movieProvidedToService = argumentCaptor.getValue();

        assertEquals(LocalDateTime.of(1111, 1, 1, 1, 1, 1), movieProvidedToService.getDate());
        assertEquals(movie, movieProvidedToService.getMovie());

        mockMvc.perform(post("/projections")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":\"1\",\"date\":\"1111-01-01T01:01:01\",\"movie_id\":\"null\"}"))
                .andExpect(status().isBadRequest());

        mockMvc.perform(post("/projections")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":\"1\",\"date\":\"1111-01-01T01:01:01\",\"movie_id\":\"13\"}"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdate() throws Exception {
        Movie movie = new Movie(11L, "11");
        Projection pro = new Projection(1L, LocalDateTime.of(1111, 1, 1, 1, 1, 1), movie);

        Mockito.when(projectionService.readById(1L)).thenReturn(Optional.of(pro));
        Mockito.when(projectionService.readById(not(eq(1L)))).thenReturn(Optional.empty());
        Mockito.when(movieService.readById(11L)).thenReturn(Optional.of(movie));
        Mockito.when(movieService.readById(not(eq(11L)))).thenReturn(Optional.empty());

        mockMvc.perform(put("/projections/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":\"13\",\"date\":\"1111-01-01T01:01:01\",\"movie_id\":\"11\"}"))
                .andExpect(status().isBadRequest());

        mockMvc.perform(put("/projections/13")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":\"13\",\"date\":\"1111-01-01T01:01:01\",\"movie_id\":\"11\"}"))
                .andExpect(status().isNotFound());

        mockMvc.perform(put("/projections/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":\"1\",\"date\":\"1111-01-01T01:01:01\",\"movie_id\":\"13\"}"))
                .andExpect(status().isNotFound());

        mockMvc.perform(put("/projections/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":\"1\",\"date\":\"1111-01-01T01:01:01\",\"movie_id\":\"11\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", Matchers.is(1)))
                .andExpect(jsonPath("$.date", Matchers.is("1111-01-01T01:01:01")))
                .andExpect(jsonPath("$.movie_id", Matchers.is(11)));

        ArgumentCaptor<Projection> argumentCaptor = ArgumentCaptor.forClass(Projection.class);
        verify(projectionService, Mockito.times(1)).update(argumentCaptor.capture());
        Projection movieProvidedToService = argumentCaptor.getValue();

        assertEquals(1L, movieProvidedToService.getId());
        assertEquals(LocalDateTime.of(1111, 1, 1, 1, 1, 1), movieProvidedToService.getDate());
        assertEquals(movie, movieProvidedToService.getMovie());
    }

    @Test
    public void testDelete() throws Exception {
        Projection pro = new Projection(1L, LocalDateTime.of(1111, 1, 1, 1, 1, 1), new Movie(1L, "Dummy"));

        Mockito.when(projectionService.readById(1L)).thenReturn(Optional.of(pro));
        Mockito.when(projectionService.readById(not(eq(1L)))).thenReturn(Optional.empty());

        mockMvc.perform(delete("/projections/13"))
                .andExpect(status().isNotFound());
        verify(projectionService, never()).deleteById(any());

        mockMvc.perform(delete("/projections/1"))
                .andExpect(status().isOk());
        verify(projectionService, times(1)).deleteById(1L);
    }
}
