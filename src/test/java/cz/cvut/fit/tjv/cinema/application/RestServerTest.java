package cz.cvut.fit.tjv.cinema.application;

import cz.cvut.fit.tjv.cinema.api.controller.CustomerController;
import cz.cvut.fit.tjv.cinema.api.controller.MovieController;
import cz.cvut.fit.tjv.cinema.api.controller.ProjectionController;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class RestServerTest {

    @Autowired
    MovieController movieController;

    @Autowired
    ProjectionController projectionController;

    @Autowired
    CustomerController customerController;

    @Test
    public void testContextLoadsController() {
        Assertions.assertThat(movieController).isNotNull();
        Assertions.assertThat(projectionController).isNotNull();
        Assertions.assertThat(customerController).isNotNull();
    }
}
