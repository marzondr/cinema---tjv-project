package cz.cvut.fit.tjv.cinema.controller;

import cz.cvut.fit.tjv.cinema.api.controller.MovieController;
import cz.cvut.fit.tjv.cinema.business.MovieService;
import cz.cvut.fit.tjv.cinema.business.ProjectionService;
import cz.cvut.fit.tjv.cinema.domain.Movie;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(MovieController.class)
public class MovieControllerTest {

    @MockBean
    MovieService movieService;

    @MockBean
    ProjectionService projectionService;

    @Autowired
    MockMvc mockMvc;

    @Test
    public void testGetAll() throws Exception {
        Movie movie1 = new Movie(1L,"Harry Potter");
        Movie movie2 = new Movie(2L,"Red Notice");
        Movie movie3 = new Movie(3L,"Jumanji");

        List<Movie> movies = List.of(movie1, movie2, movie3);

        Mockito.when(movieService.readAll()).thenReturn(movies);

        mockMvc.perform(get("/movies"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(3)))
                .andExpect(jsonPath("$[0].id", Matchers.is(1)))
                .andExpect(jsonPath("$[1].id", Matchers.is(2)))
                .andExpect(jsonPath("$[2].id", Matchers.is(3)))
                .andExpect(jsonPath("$[0].name", Matchers.is("Harry Potter")))
                .andExpect(jsonPath("$[1].name", Matchers.is("Red Notice")))
                .andExpect(jsonPath("$[2].name", Matchers.is("Jumanji")));
    }

    @Test
    public void testGetOne() throws Exception {
        Movie movie = new Movie(17L, "Lord of the Rings");

        Mockito.when(movieService.readById(17L)).thenReturn(Optional.of(movie));
        Mockito.when(movieService.readById(not(eq(17L)))).thenReturn(Optional.empty());

        mockMvc.perform(get("/movies/17"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", Matchers.is(17)))
                .andExpect(jsonPath("$.name", Matchers.is("Lord of the Rings")));

        mockMvc.perform(get("/movies/13"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testCreate() throws Exception {
        Movie movie = new Movie(17L, "Lord of the Rings");

        Mockito.when(movieService.readById(any())).thenReturn(Optional.of(movie));

        mockMvc.perform(post("/movies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":\"17\",\"name\":\"Lord of the Rings\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", Matchers.is("Lord of the Rings")));

        ArgumentCaptor<Movie> argumentCaptor = ArgumentCaptor.forClass(Movie.class);
        Mockito.verify(movieService, Mockito.times(1)).create(argumentCaptor.capture());
        Movie movieProvidedToService = argumentCaptor.getValue();

        assertEquals("Lord of the Rings", movieProvidedToService.getName());
    }

    @Test
    public void testUpdate() throws Exception {
        Movie movie = new Movie(17L, "Lord of the Rings");

        Mockito.when(movieService.readById(17L)).thenReturn(Optional.of(movie));
        Mockito.when(movieService.readById(not(eq(17L)))).thenReturn(Optional.empty());

        mockMvc.perform(put("/movies/13")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":\"13\",\"name\":\"Lord of the Rings\"}"))
                .andExpect(status().isNotFound());

        mockMvc.perform(put("/movies/13")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":\"17\",\"name\":\"Lord of the Rings\"}"))
                .andExpect(status().isBadRequest());

        mockMvc.perform(put("/movies/17")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":\"17\",\"name\":\"Lord of the Rings\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", Matchers.is(17)))
                .andExpect(jsonPath("$.name", Matchers.is("Lord of the Rings")));

        ArgumentCaptor<Movie> argumentCaptor = ArgumentCaptor.forClass(Movie.class);
        Mockito.verify(movieService, Mockito.times(1)).update(argumentCaptor.capture());
        Movie movieProvidedToService = argumentCaptor.getValue();

        assertEquals(17L, movieProvidedToService.getId());
        assertEquals("Lord of the Rings", movieProvidedToService.getName());
    }

    @Test
    public void testDelete() throws Exception {
        Movie movie = new Movie(17L, "Lord of the Rings");

        Mockito.when(movieService.readById(17L)).thenReturn(Optional.of(movie));
        Mockito.when(movieService.readById(not(eq(17L)))).thenReturn(Optional.empty());

        mockMvc.perform(delete("/movies/13"))
                .andExpect(status().isNotFound());
        verify(movieService, never()).deleteById(any());

        mockMvc.perform(delete("/movies/17"))
                .andExpect(status().isOk());
        verify(movieService, times(1)).deleteById(17L);
    }
}
