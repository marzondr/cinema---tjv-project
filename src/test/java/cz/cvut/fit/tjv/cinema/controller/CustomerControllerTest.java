package cz.cvut.fit.tjv.cinema.controller;

import cz.cvut.fit.tjv.cinema.api.controller.CustomerController;
import cz.cvut.fit.tjv.cinema.business.CustomerService;
import cz.cvut.fit.tjv.cinema.business.ProjectionService;
import cz.cvut.fit.tjv.cinema.domain.Customer;
import cz.cvut.fit.tjv.cinema.domain.Movie;
import cz.cvut.fit.tjv.cinema.domain.Projection;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CustomerController.class)
public class CustomerControllerTest {

    @MockBean
    CustomerService customerService;

    @MockBean
    ProjectionService projectionService;

    @Autowired
    MockMvc mockMvc;

    @Test
    public void testGetAll() throws Exception {
        Customer cus1 = new Customer(1L,"Dwayne Johnson");
        Customer cus2 = new Customer(2L,"Daniel Radcliff");
        Customer cus3 = new Customer(3L,"Emma Stone");

        List<Customer> customers = List.of(cus1, cus2, cus3);

        Mockito.when(customerService.readAll()).thenReturn(customers);

        mockMvc.perform(get("/users"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(3)))
                .andExpect(jsonPath("$[0].id", Matchers.is(1)))
                .andExpect(jsonPath("$[1].id", Matchers.is(2)))
                .andExpect(jsonPath("$[2].id", Matchers.is(3)))
                .andExpect(jsonPath("$[0].username", Matchers.is("Dwayne Johnson")))
                .andExpect(jsonPath("$[1].username", Matchers.is("Daniel Radcliff")))
                .andExpect(jsonPath("$[2].username", Matchers.is("Emma Stone")));
    }

    @Test
    public void testGetOne() throws Exception {
        Customer cus = new Customer(17L, "Tester Testovič");

        Mockito.when(customerService.readById(17L)).thenReturn(Optional.of(cus));
        Mockito.when(customerService.readById(not(eq(17L)))).thenReturn(Optional.empty());

        mockMvc.perform(get("/users/17"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", Matchers.is(17)))
                .andExpect(jsonPath("$.username", Matchers.is("Tester Testovič")));

        mockMvc.perform(get("/users/13"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetUserProjections() throws Exception {
        Customer cus = new Customer(17L, "Tester Testovič");

        Projection pro1 = new Projection(2L, LocalDateTime.of(2022, 2, 22, 22, 22, 22), new Movie(2L, "22"));

        List<Long> projection_ids = List.of(1L, 2L, 3L);

        Mockito.when(customerService.readById(17L)).thenReturn(Optional.of(cus));
        Mockito.when(customerService.readById(not(eq(17L)))).thenReturn(Optional.empty());

        Mockito.when(customerService.getTicketsByUser(17L)).thenReturn(projection_ids);

        Mockito.when(projectionService.readById(1L)).thenReturn(Optional.of(pro1));
        Mockito.when(projectionService.readById(not(eq(1L)))).thenReturn(Optional.empty());

        mockMvc.perform(get("/users/17/projections"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(1)))
                .andExpect(jsonPath("$[0].id", Matchers.is(2)))
                .andExpect(jsonPath("$[0].date", Matchers.is("2022-02-22T22:22:22")))
                .andExpect(jsonPath("$[0].movie_id", Matchers.is(2)));

        mockMvc.perform(get("/users/13/projections"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testCreate() throws Exception {
        Customer cus = new Customer(17L, "Tester Testovič");

        Mockito.when(customerService.readById(any())).thenReturn(Optional.of(cus));

        mockMvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":\"17\",\"username\":\"Tester Testovič\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username", Matchers.is("Tester Testovič")));

        ArgumentCaptor<Customer> argumentCaptor = ArgumentCaptor.forClass(Customer.class);
        Mockito.verify(customerService, Mockito.times(1)).create(argumentCaptor.capture());
        Customer movieProvidedToService = argumentCaptor.getValue();

        assertEquals("Tester Testovič", movieProvidedToService.getUsername());
    }

    @Test
    public void testAddProjectionToUser() throws Exception {
        Customer cus = new Customer(17L, "Tester Testovič");
        Projection pro = new Projection(2L, LocalDateTime.of(2022, 2, 22, 22, 22, 22), new Movie(1L, "Dummy"));

        Mockito.when(customerService.readById(17L)).thenReturn(Optional.of(cus));
        Mockito.when(customerService.readById(not(eq(17L)))).thenReturn(Optional.empty());
        Mockito.when(projectionService.readById(2L)).thenReturn(Optional.of(pro));
        Mockito.when(projectionService.readById(not(eq(2L)))).thenReturn(Optional.empty());

        mockMvc.perform(post("/users/13/projections/13")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(""))
                .andExpect(status().isBadRequest());

        mockMvc.perform(post("/users/17/projections/13")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(""))
                .andExpect(status().isBadRequest());

        mockMvc.perform(post("/users/17/projections/2")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(""))
                .andExpect(status().isOk());

        Mockito.verify(customerService, Mockito.times(1)).addTicket(any(), any());
    }

    @Test
    public void testUpdate() throws Exception {
        Customer cus = new Customer(17L, "Tester Testovič");

        Mockito.when(customerService.readById(17L)).thenReturn(Optional.of(cus));
        Mockito.when(customerService.readById(not(eq(17L)))).thenReturn(Optional.empty());

        mockMvc.perform(put("/users/13")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":\"13\",\"username\":\"Tester Testovič\"}"))
                .andExpect(status().isNotFound());

        mockMvc.perform(put("/users/13")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":\"17\",\"username\":\"Tester Testovič\"}"))
                .andExpect(status().isBadRequest());

        mockMvc.perform(put("/users/17")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":\"17\",\"username\":\"Tester Testovič\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", Matchers.is(17)))
                .andExpect(jsonPath("$.username", Matchers.is("Tester Testovič")));

        ArgumentCaptor<Customer> argumentCaptor = ArgumentCaptor.forClass(Customer.class);
        Mockito.verify(customerService, Mockito.times(1)).update(argumentCaptor.capture());
        Customer movieProvidedToService = argumentCaptor.getValue();

        assertEquals(17L, movieProvidedToService.getId());
        assertEquals("Tester Testovič", movieProvidedToService.getUsername());
    }

    @Test
    public void testDelete() throws Exception {
        Customer cus = new Customer(17L, "Tester Testovič");

        Mockito.when(customerService.readById(17L)).thenReturn(Optional.of(cus));
        Mockito.when(customerService.readById(not(eq(17L)))).thenReturn(Optional.empty());

        mockMvc.perform(delete("/users/13"))
                .andExpect(status().isNotFound());
        verify(customerService, never()).deleteById(any());

        mockMvc.perform(delete("/users/17"))
                .andExpect(status().isOk());
        verify(customerService, times(1)).deleteById(17L);
    }

    @Test
    public void testDeleteOneTicket() throws Exception {
        Customer cus = new Customer(17L, "Tester Testovič");

        Mockito.when(customerService.readById(17L)).thenReturn(Optional.of(cus));
        Mockito.when(customerService.readById(not(eq(17L)))).thenReturn(Optional.empty());

        Projection pro = new Projection(13L, LocalDateTime.of(2000, 2, 2, 22, 22, 22), new Movie(1L, "Dummy"));

        Mockito.when(projectionService.readById(13L)).thenReturn(Optional.of(pro));
        Mockito.when(projectionService.readById(not(eq(13L)))).thenReturn(Optional.empty());

        mockMvc.perform(delete("/users/13/projections/13"))
                .andExpect(status().isNotFound());
        verify(customerService, never()).deleteOneTicket(any(), any());

        mockMvc.perform(delete("/users/17/projections/17"))
                .andExpect(status().isNotFound());
        verify(customerService, never()).deleteOneTicket(any(), any());

        mockMvc.perform(delete("/users/17/projections/13"))
                .andExpect(status().isOk());
        verify(customerService, times(1)).deleteOneTicket(any(), any());
    }
}
