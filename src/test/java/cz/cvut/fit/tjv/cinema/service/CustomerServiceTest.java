package cz.cvut.fit.tjv.cinema.service;

import cz.cvut.fit.tjv.cinema.business.CustomerService;
import cz.cvut.fit.tjv.cinema.business.EntityStateException;
import cz.cvut.fit.tjv.cinema.dao.CustomerJpaRepository;
import cz.cvut.fit.tjv.cinema.domain.Customer;
import cz.cvut.fit.tjv.cinema.domain.Movie;
import cz.cvut.fit.tjv.cinema.domain.Projection;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceTest {

    @InjectMocks
    CustomerService customerService;

    @Mock
    CustomerJpaRepository repository;

    @Test
    public void testReadAll() {
        Customer cus1 = new Customer(1L,"Dwayne Johnson");
        Customer cus2 = new Customer(2L,"Daniel Radcliff");
        Customer cus3 = new Customer(3L,"Emma Stone");

        List<Customer> customers = List.of(cus1, cus2, cus3);

        Mockito.when(repository.findAll()).thenReturn(customers);

        Collection<Customer> returnedCustomers = repository.findAll();

        assertEquals(3, returnedCustomers.size());
        verify(repository, times(1)).findAll();
    }

    @Test
    public void testGetTicketsByUser() {
        Customer cus = new Customer(17L, "Tester Testovič");
        List<Long> ticketIds = List.of(1L, 2L, 3L);

        Mockito.when(repository.getTicketsByUser(cus.getId())).thenReturn(ticketIds);

        Collection<Long> returnedTicketIds = customerService.getTicketsByUser(cus.getId());

        assertEquals(3, returnedTicketIds.size());
        verify(repository, times(1)).getTicketsByUser(17L);
    }

    @Test
    public void testReadById() {
        Customer cus = new Customer(17L, "Tester Testovič");

        Mockito.when(repository.findById(17L)).thenReturn(Optional.of(cus));
        Mockito.when(repository.findById(not(eq(17L)))).thenReturn(Optional.empty());

        Optional<Customer> returnedCustomer = customerService.readById(17L);
        Optional<Customer> retEmptyCustomer = customerService.readById(13L);

        assertTrue(returnedCustomer.isPresent());
        assertTrue(retEmptyCustomer.isEmpty());

        assertEquals(cus, returnedCustomer.get());
        verify(repository, times(2)).findById(any());
    }

    @Test
    public void testCreate() {
        Customer cus = new Customer(17L, "Tester Testovič");
        repository.save(cus);
        verify(repository, times(1)).save(cus);
    }

    @Test
    public void testAddTicket() {
        Customer cus = new Customer(17L, "Tester Testovič");
        Projection pro = new Projection(1L, LocalDateTime.of(1111, 1, 1, 1, 1, 1), new Movie(11L, "11"));

        repository.addTicket(cus.getId(), pro.getId());
        verify(repository, times(1)).addTicket(cus.getId(), pro.getId());
    }

    @Test
    public void testUpdate() throws EntityStateException {
        Customer cus = new Customer(17L, "Tester Testovič");

        Mockito.when(repository.existsById(17L)).thenReturn(true);
        Mockito.when(repository.existsById(not(eq(17L)))).thenReturn(false);

        try {
            customerService.update(new Customer(13L, "Tester Testovič"));
        } catch (EntityStateException e) {
            verify(repository, never()).save(any());
        }

        customerService.update(cus);
        verify(repository, times(1)).save(cus);
    }

    @Test
    public void testDeleteById() {
        customerService.deleteById(17L);
        verify(repository, never()).deleteById(not(eq(17L)));
        verify(repository, times(1)).deleteById(17L);
    }

    @Test
    public void testDeleteOneTicket() {
        customerService.deleteOneTicket(17L, 1L);
        verify(repository, never()).deleteOneTicket(not(eq(17L)), not(eq(1L)));
        verify(repository, times(1)).deleteOneTicket(17L, 1L);
    }
}
