package cz.cvut.fit.tjv.cinema.api.converter;

import cz.cvut.fit.tjv.cinema.api.dto.CustomerDTO;
import cz.cvut.fit.tjv.cinema.domain.Customer;
import cz.cvut.fit.tjv.cinema.domain.Projection;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class CustomerConverter {

    public CustomerConverter() {
    }

    public static Customer toModel(CustomerDTO customerDTO) {
        return new Customer( customerDTO.getUsername() );
    }

    public static Customer toModelWithId(CustomerDTO customerDTO) {
        return new Customer ( customerDTO.getId(), customerDTO.getUsername() );
    }

    public static CustomerDTO fromModel(Customer customer) {

        Set<Long> ticketsIds = new HashSet<>();

        for (Projection i: customer.getTickets())
            ticketsIds.add(i.getId());

        return new CustomerDTO(customer.getId(), customer.getUsername(), ticketsIds);
    }

    public static Collection<Customer> toModels(Collection<CustomerDTO> customerDTOs) {
        return customerDTOs.stream().map(CustomerConverter::toModel).toList();
    }
    public static Collection<CustomerDTO> fromModels(Collection<Customer> customers) {
        return customers.stream().map(CustomerConverter::fromModel).toList();
    }

}
