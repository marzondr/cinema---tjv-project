package cz.cvut.fit.tjv.cinema.api.converter;

import cz.cvut.fit.tjv.cinema.api.dto.MovieDTO;
import cz.cvut.fit.tjv.cinema.domain.Movie;

import java.util.Collection;

public class MovieConverter {

    public MovieConverter() {
    }

    public static Movie toModel (MovieDTO movieDTO) {
        return new Movie(movieDTO.getName());
    }

    public static Movie toModelWithId (MovieDTO movieDTO) {
        return new Movie(movieDTO.getId(), movieDTO.getName());
    }

    public static MovieDTO fromModel(Movie movie) {
        return new MovieDTO(movie.getId(), movie.getName());
    }

    public static Collection<Movie> toModels(Collection<MovieDTO> movieDTOs) {
        return movieDTOs.stream().map(MovieConverter::toModel).toList();
    }
    public static Collection<MovieDTO> fromModels(Collection<Movie> movies) {
        return movies.stream().map(MovieConverter::fromModel).toList();
    }

}
