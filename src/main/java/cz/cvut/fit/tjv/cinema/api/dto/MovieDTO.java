package cz.cvut.fit.tjv.cinema.api.dto;

public class MovieDTO {

    private Long id;
    private String name;

    public MovieDTO() {}

    public MovieDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() { return id; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
