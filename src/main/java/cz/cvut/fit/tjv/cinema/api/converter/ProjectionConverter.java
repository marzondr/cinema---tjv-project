package cz.cvut.fit.tjv.cinema.api.converter;

import cz.cvut.fit.tjv.cinema.api.dto.ProjectionDTO;
import cz.cvut.fit.tjv.cinema.domain.Movie;
import cz.cvut.fit.tjv.cinema.domain.Projection;

import java.util.Collection;

public class ProjectionConverter {

    public ProjectionConverter() {
    }

    public static Projection toModel(ProjectionDTO projectionDTO, Movie movie) {
        return new Projection( projectionDTO.getDate(), movie );
    }

    public static Projection toModelWithId (ProjectionDTO projectionDTO, Movie movie) {
        return new Projection( projectionDTO.getId(), projectionDTO.getDate(), movie );
    }

    public static ProjectionDTO fromModel(Projection projection) {
        return new ProjectionDTO( projection.getId(),
                                  projection.getDate(),
                                  projection.getMovie().getId());
    }

    public static Collection<ProjectionDTO> fromModels(Collection<Projection> projections) {
        return projections.stream().map(ProjectionConverter::fromModel).toList();
    }

}
