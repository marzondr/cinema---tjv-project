package cz.cvut.fit.tjv.cinema.api.dto;

import java.util.HashSet;
import java.util.Set;

public class CustomerDTO {

    private Long id;
    private String username;
    private Set<Long> tickets = new HashSet<>();

    public CustomerDTO() {}

    public CustomerDTO(Long id, String username, Set<Long> tickets) {
        this.id = id;
        this.username = username;
        this.tickets = tickets;
    }

    public Long getId() { return id; }

    public String getUsername() {
        return username;
    }

    public Set<Long> getTickets() { return tickets; }

    public void setId(Long id) {this.id = id; }
    public void setUsername(String username) {
        this.username = username;
    }
}
