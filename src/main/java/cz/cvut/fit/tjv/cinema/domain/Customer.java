package cz.cvut.fit.tjv.cinema.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity //(name = "customer")
public class Customer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String username;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "customer_projections",
            joinColumns = {@JoinColumn(name = "customer_id")},
            inverseJoinColumns = {@JoinColumn(name = "projection_id")}
    )
    private Set<Projection> tickets = new HashSet<>();

    public Customer() { }

    public Customer( String username ) {
        this.username = Objects.requireNonNull(username);
    }

    public Customer(Long id, String username) {
        this.id = Objects.requireNonNull(id);
        this.username = Objects.requireNonNull(username);
    }

    public Long getId() { return id; }

    public String getUsername() {
        return username;
    }

    public Set<Projection> getTickets() { return tickets; }

    public void addTicket( Projection ticket ) { tickets.add( ticket ); }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id + "," +
                "username='" + username + '\'' +
                '}';
    }
}
