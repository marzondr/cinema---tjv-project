package cz.cvut.fit.tjv.cinema.business;

import cz.cvut.fit.tjv.cinema.dao.CustomerJpaRepository;
import cz.cvut.fit.tjv.cinema.domain.Customer;
import cz.cvut.fit.tjv.cinema.domain.Projection;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Optional;

@Component
public class CustomerService {

    protected final CustomerJpaRepository repository;

    protected CustomerService(CustomerJpaRepository repository) {
        this.repository = repository;
    }

    public Collection<Customer> readAll() {
        return repository.findAll();
    }

    public Collection<Long> getTicketsByUser(Long id) {
        return repository.getTicketsByUser(id);
    }

    public Optional<Customer> readById(Long id) {
        return repository.findById(id);
    }

    public void create(Customer entity) throws EntityStateException {
        repository.save(entity);
    }

    public void addTicket(Customer customer, Projection projection) { repository.addTicket(customer.getId(), projection.getId()); }

    public void update(Customer entity) throws EntityStateException {
        if (repository.existsById(entity.getId()))
            repository.save(entity);
        else
            throw new EntityStateException(entity);
    }

    public void deleteById(Long id) {
        repository.deleteTickets(id);
        repository.deleteById(id);
    }

    public void deleteOneTicket(Long idU, Long idP) {
        repository.deleteOneTicket(idU, idP);
    }
}
