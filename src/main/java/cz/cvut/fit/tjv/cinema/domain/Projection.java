package cz.cvut.fit.tjv.cinema.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Projection implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private LocalDateTime date;

    @ManyToMany(mappedBy = "tickets", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private Set<Customer> customers = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "movie_id", nullable = false)
    private Movie movie;

    public Projection() { }

    public Projection(LocalDateTime date, Movie movie) {
        this.date = Objects.requireNonNull(date);
        this.movie = Objects.requireNonNull(movie);
    }

    public Projection(Long id, LocalDateTime date, Movie movie) {
        this.id = Objects.requireNonNull(id);
        this.date = Objects.requireNonNull(date);
        this.movie = Objects.requireNonNull(movie);
    }

    public Long getId() { return id; }

    public LocalDateTime getDate() {
        return date;
    }

    public Movie getMovie() { return movie; }

    public void setMovie( Movie movie ) { this.movie = movie; }

    public void addCustomer( Customer customer ) { customers.add( customer ); }

    @Override
    public String toString() {
        return "Projection{" +
                "id=" + id + "," +
                "date=" + date + "," +
                "movie=" + movie + '\'' +
                '}';
    }
}
