package cz.cvut.fit.tjv.cinema.business;

public class AccessDeniedException extends RuntimeException {
    public AccessDeniedException(String s) {
        super(s);
    }
}
