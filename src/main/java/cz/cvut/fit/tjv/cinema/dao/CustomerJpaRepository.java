package cz.cvut.fit.tjv.cinema.dao;

import cz.cvut.fit.tjv.cinema.domain.Customer;
import cz.cvut.fit.tjv.cinema.domain.Projection;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface CustomerJpaRepository extends JpaRepository<Customer, Long> {

    @Query(nativeQuery = true, value = "SELECT projection_id FROM customer_projections WHERE customer_id = :id" )
    List<Long> getTicketsByUser(@Param("id") Long id);

    @Modifying
    @Query(nativeQuery = true, value = "INSERT INTO customer_projections (customer_id, projection_id) values (:customer_id, :projection_id)")
    @Transactional
    void addTicket(@Param("customer_id") Long customer_id, @Param("projection_id") Long projection_id);

    @Modifying
    @Query(nativeQuery = true, value = "DELETE FROM customer_projections WHERE customer_id = :id")
    @Transactional
    void deleteTickets(@Param("id") Long id);

    @Modifying
    @Query(nativeQuery = true, value = "DELETE FROM customer_projections WHERE customer_id = :idU AND projection_id = :idP")
    @Transactional
    void deleteOneTicket(@Param("idU") Long idU, @Param("idP") Long idP);

    @Override
    Optional<Customer> findById(Long aLong);

    @Override
    List<Customer> findAll();

    @Override
    <S extends Customer> S save(S entity);

    @Override
    void deleteById(Long aLong);

    @Override
    void delete(Customer entity);

    @Override
    boolean existsById(Long aLong);

    @Override
    <S extends Customer> boolean exists(Example<S> example);
}
