package cz.cvut.fit.tjv.cinema.api.dto;

import java.time.LocalDateTime;

public class ProjectionDTO {

    private Long id;
    private LocalDateTime date;
    private Long movie_id;

    public ProjectionDTO() {}

    public ProjectionDTO(Long id, LocalDateTime date, Long movie_id) {
        this.id = id;
        this.date = date;
        this.movie_id = movie_id;
    }

    public Long getId() { return id; }

    public LocalDateTime getDate() {
        return date;
    }

    public Long getMovie_id() { return movie_id; }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
}
