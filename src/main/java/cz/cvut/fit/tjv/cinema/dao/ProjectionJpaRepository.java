package cz.cvut.fit.tjv.cinema.dao;

import cz.cvut.fit.tjv.cinema.domain.Movie;
import cz.cvut.fit.tjv.cinema.domain.Projection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Collection;


@Repository
public interface ProjectionJpaRepository extends JpaRepository<Projection, Long> {

    @Modifying
    @Query(nativeQuery = true, value = "DELETE FROM customer_projections WHERE projection_id = :id")
    @Transactional
    void deleteTickets(@Param("id") Long id);

    @Query(nativeQuery = true, value = "SELECT * FROM Projection WHERE movie_id = :id")
    Collection<Projection> readByMovieId(@Param("id") Long id);

    @Override
    <S extends Projection> S save(S entity);

    @Override
    boolean existsById(Long aLong);


}
