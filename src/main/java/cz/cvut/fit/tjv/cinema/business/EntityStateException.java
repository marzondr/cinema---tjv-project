package cz.cvut.fit.tjv.cinema.business;

public class EntityStateException extends Exception {
    public EntityStateException() {
    }

    public <E> EntityStateException(E entity) {
        super("Illegal state of entity " + entity);
    }
}
