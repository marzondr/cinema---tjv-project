package cz.cvut.fit.tjv.cinema.api.controller;

import cz.cvut.fit.tjv.cinema.api.converter.CustomerConverter;
import cz.cvut.fit.tjv.cinema.api.converter.ProjectionConverter;
import cz.cvut.fit.tjv.cinema.api.dto.CustomerDTO;
import cz.cvut.fit.tjv.cinema.api.dto.ProjectionDTO;
import cz.cvut.fit.tjv.cinema.business.EntityStateException;
import cz.cvut.fit.tjv.cinema.business.CustomerService;
import cz.cvut.fit.tjv.cinema.business.ProjectionService;
import cz.cvut.fit.tjv.cinema.domain.Customer;
import cz.cvut.fit.tjv.cinema.domain.Projection;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
public class CustomerController {

    private final CustomerService customerService;
    private final ProjectionService projectionService;

    public CustomerController(CustomerService customerService, ProjectionService projectionService) {
        this.customerService = customerService;
        this.projectionService = projectionService;
    }

    @GetMapping("/users")
    Collection<CustomerDTO> getAll() {
        return CustomerConverter.fromModels(customerService.readAll());
    }

    @GetMapping("/users/{id}")
    CustomerDTO getOne(@PathVariable Long id) {
        return CustomerConverter.fromModel(customerService.readById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found")
        ));
    }

    @GetMapping("/users/{id}/projections")
    Collection<ProjectionDTO> getUserProjections(@PathVariable Long id) {
        Optional<Customer> customer = customerService.readById(id);
        if (customer.isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
        Collection<Long> projections_ids = customerService.getTicketsByUser(id);

        List<Projection> projections = new ArrayList<>();
        projections_ids.forEach(p_id -> {
            Optional<Projection> projection = projectionService.readById(p_id);
            projection.ifPresent(projections::add);
        });
        return ProjectionConverter.fromModels( projections );
    }

    @PostMapping("/users")
    CustomerDTO create(@RequestBody CustomerDTO customerDTO) {
        Customer customerModel = CustomerConverter.toModel( customerDTO );
        try {
            customerService.create( customerModel );
        } catch (EntityStateException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User already exists");
        }
        return getOne( customerModel.getId() );
    }

    @PostMapping("/users/{idU}/projections/{idP}")
    CustomerDTO addProjectionToUser(@PathVariable Long idU, @PathVariable Long idP) {
        Optional<Customer> customer = customerService.readById(idU);
        Optional<Projection> projection = projectionService.readById(idP);

        if (customer.isEmpty() || projection.isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User or projection does not exist");

        if (customer.get().getTickets().contains(projection.get()))
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Ticket already owned");

        customerService.addTicket(customer.get(), projection.get());
        return getOne(idU);
    }

    @PutMapping("/users/{id}")
    CustomerDTO update(@PathVariable Long id, @RequestBody CustomerDTO customerDTO) {
        if(!customerDTO.getId().equals(id))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User ids do not match");
        try {
            if (customerService.readById(id).isPresent() )
                customerService.update( CustomerConverter.toModelWithId(customerDTO) );

        } catch (EntityStateException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
        }
        return getOne(customerDTO.getId());
    }

    @DeleteMapping("/users/{id}")
    void delete(@PathVariable Long id) {
        if(customerService.readById(id).isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
        customerService.deleteById(id);
        // also does:  .deleteUserTickets(id);
    }

    @DeleteMapping("/users/{idU}/projections/{idP}")
    void deleteOneTicket(@PathVariable Long idU, @PathVariable Long idP) {
        if (customerService.readById(idU).isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
        if (projectionService.readById(idP).isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Projection not found");
        customerService.deleteOneTicket(idU, idP);
    }
}
