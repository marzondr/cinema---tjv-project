package cz.cvut.fit.tjv.cinema.api.controller;

import cz.cvut.fit.tjv.cinema.api.converter.MovieConverter;
import cz.cvut.fit.tjv.cinema.api.dto.MovieDTO;
import cz.cvut.fit.tjv.cinema.business.EntityStateException;
import cz.cvut.fit.tjv.cinema.business.MovieService;
import cz.cvut.fit.tjv.cinema.business.ProjectionService;
import cz.cvut.fit.tjv.cinema.domain.Movie;
import cz.cvut.fit.tjv.cinema.domain.Projection;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;

@CrossOrigin
@RestController
public class MovieController {

    private final MovieService movieService;
    private final ProjectionService projectionService;

    public MovieController(MovieService movieService, ProjectionService projectionService) {
        this.movieService = movieService;
        this.projectionService = projectionService;
    }

    @GetMapping("/movies")
    Collection<MovieDTO> getAll() {
        return MovieConverter.fromModels(movieService.readAll());
    }

    @GetMapping("/movies/{id}")
    MovieDTO getOne(@PathVariable Long id) { return MovieConverter.fromModel(movieService.readById(id).orElseThrow(
            () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Movie not found")
        ));
    }

    @PostMapping("/movies")
    public MovieDTO create(@RequestBody MovieDTO movieDTO) {
        Movie movieModel = MovieConverter.toModel(movieDTO);
        try {
            movieService.create( movieModel );
        } catch (EntityStateException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Movie already exists");
        }
        return getOne( movieModel.getId() );
    }

    @PutMapping("/movies/{id}")
    MovieDTO update(@PathVariable Long id, @RequestBody MovieDTO movieDTO) {
        if(!movieDTO.getId().equals(id))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Movie ids do not match");
        try {
            if (movieService.readById(id).isPresent())
                movieService.update( MovieConverter.toModelWithId(movieDTO) );

        } catch (EntityStateException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Movie not found");
        }
        return getOne(movieDTO.getId());
    }

    @DeleteMapping("/movies/{id}")
    void delete(@PathVariable Long id) {
        if(movieService.readById(id).isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Movie not found");
        Collection<Projection> projections = projectionService.readByMovieId(id);
        System.out.println(projections);

        projections.forEach(p -> {
            projectionService.deleteTickets(p.getId());
            projectionService.deleteById(p.getId());
        });
        movieService.deleteById(id);
    }
}
