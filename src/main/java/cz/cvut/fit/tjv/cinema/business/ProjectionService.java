package cz.cvut.fit.tjv.cinema.business;

import cz.cvut.fit.tjv.cinema.dao.ProjectionJpaRepository;
import cz.cvut.fit.tjv.cinema.domain.Projection;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Optional;

@Component
public class ProjectionService {

    protected final ProjectionJpaRepository repository;

    protected ProjectionService(ProjectionJpaRepository repository) {
        this.repository = repository;
    }

    public Collection<Projection> readAll() {
        return repository.findAll();
    }

    public Collection<Projection> readByMovieId(Long id) {
        return repository.readByMovieId(id);
    }

    public Optional<Projection> readById(Long id) {
        return repository.findById(id);
    }

    public void create(Projection entity) throws EntityStateException {
        repository.save(entity);
    }

    public void update(Projection entity) throws EntityStateException {
        if (repository.existsById(entity.getId()))
            repository.save(entity);
        else
            throw new EntityStateException(entity);
    }

    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    public void deleteTickets(Long id) {
        repository.deleteTickets(id);
    }
}
