package cz.cvut.fit.tjv.cinema.dao;

import cz.cvut.fit.tjv.cinema.domain.Movie;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface MovieJpaRepository extends JpaRepository<Movie, Long> {

    @Override
    Optional<Movie> findById(Long aLong);

    @Override
    List<Movie> findAll();

    @Override
    <S extends Movie> S save(S entity);

    @Override
    void deleteById(Long aLong);
}
