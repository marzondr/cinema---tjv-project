package cz.cvut.fit.tjv.cinema.api.controller;

import cz.cvut.fit.tjv.cinema.api.converter.ProjectionConverter;
import cz.cvut.fit.tjv.cinema.api.dto.ProjectionDTO;
import cz.cvut.fit.tjv.cinema.business.EntityStateException;
import cz.cvut.fit.tjv.cinema.business.MovieService;
import cz.cvut.fit.tjv.cinema.business.ProjectionService;
import cz.cvut.fit.tjv.cinema.domain.Movie;
import cz.cvut.fit.tjv.cinema.domain.Projection;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;
import java.util.Optional;

@CrossOrigin
@RestController
public class ProjectionController {

    private final ProjectionService projectionService;
    private final MovieService movieService;

    ProjectionController( ProjectionService projectionService, MovieService movieService) {
        this.projectionService = projectionService;
        this.movieService = movieService;
    }

    @GetMapping("/projections")
    Collection<ProjectionDTO> getAll() { return ProjectionConverter.fromModels(projectionService.readAll()); }

    @GetMapping("/projections/{id}")
    ProjectionDTO getOne(@PathVariable Long id) {
        return ProjectionConverter.fromModel(projectionService.readById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Projection not found")
        ));
    }

    @PostMapping("/projections")
    ProjectionDTO create(@RequestBody ProjectionDTO projectionDTO) {
        if (projectionDTO.getMovie_id() == null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Projection movie set to null");
        Optional<Movie> movieModel = movieService.readById( projectionDTO.getMovie_id() );
        if (movieModel.isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Projection needs an existing movie");
        Projection projectionModel = ProjectionConverter.toModel(projectionDTO, movieModel.get());

        try {
            projectionService.create( projectionModel );
        } catch (EntityStateException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Projection already exists");
        }
        return getOne( projectionModel.getId() );
    }

    @PutMapping("/projections/{id}")
    ProjectionDTO update(@PathVariable Long id, @RequestBody ProjectionDTO projectionDTO) {
        if(!projectionDTO.getId().equals(id))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Projection ids do not match");

        Optional<Projection> projectionModel = projectionService.readById(projectionDTO.getId());
        Optional<Movie> movieModel = movieService.readById(projectionDTO.getMovie_id());
        if ( projectionModel.isEmpty() || movieModel.isEmpty() )
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Projection needs existing Movie");

        try {
            if (projectionService.readById(id).isPresent())
                projectionService.update( ProjectionConverter.toModelWithId(projectionDTO, movieModel.get()) );

        } catch (EntityStateException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Projection not found");
        }
        return getOne(projectionDTO.getId());
    }

    @DeleteMapping("/projections/{id}")
    void delete(@PathVariable Long id) {
        if(projectionService.readById(id).isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Projection not found");

        projectionService.deleteById(id);
        projectionService.deleteTickets(id);
    }
}
