package cz.cvut.fit.tjv.cinema.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Movie implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;

    @OneToMany(mappedBy = "movie")
    private Set<Projection> projections = new HashSet<>();

    public Movie() { }

    public Movie( String name ) {
        this.name = Objects.requireNonNull(name);
    }

    public Movie(Long id, String name) {
        this.id = Objects.requireNonNull(id);
        this.name = Objects.requireNonNull(name);
    }

    public Long getId() { return id; }

    public String getName() {
        return name;
    }

    public void addProjection( Projection projection ) { projections.add(projection); }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id + "," +
                "name='" + name + '\'' +
                '}';
    }
}
