package cz.cvut.fit.tjv.cinema.business;

import cz.cvut.fit.tjv.cinema.dao.MovieJpaRepository;
import cz.cvut.fit.tjv.cinema.domain.Movie;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Optional;

@Component
public class MovieService {

    protected final MovieJpaRepository repository;

    protected MovieService(MovieJpaRepository repository) {
        this.repository = repository;
    }

    public void create(Movie entity) throws EntityStateException {
        repository.save(entity);
    }

    public Optional<Movie> readById(Long id) {
        return repository.findById(id);
    }

    public Collection<Movie> readAll() {
        return repository.findAll();
    }

    public void update(Movie entity) throws EntityStateException {
        if (repository.existsById(entity.getId()))
            repository.save(entity);
        else
            throw new EntityStateException(entity);
    }

    public void deleteById(Long id) {
        repository.deleteById(id);
    }
}
